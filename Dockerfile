FROM maven:3.6.1-jdk-13-alpine as builder

COPY /custom-openapi-generator/pom.xml /custom-openapi-generator/pom.xml
COPY /custom-openapi-generator/modules/openapi-generator/pom.xml /custom-openapi-generator/modules/openapi-generator/pom.xml
COPY /custom-openapi-generator/modules/openapi-generator-cli/pom.xml /custom-openapi-generator/modules/openapi-generator-cli/pom.xml
COPY /custom-openapi-generator/modules/openapi-generator-core/pom.xml /custom-openapi-generator/modules/openapi-generator-core/pom.xml
COPY /custom-openapi-generator/modules/openapi-generator-maven-plugin/pom.xml /custom-openapi-generator/modules/openapi-generator-maven-plugin/pom.xml
COPY /custom-openapi-generator/modules/openapi-generator-gradle-plugin/pom.xml /custom-openapi-generator/modules/openapi-generator-gradle-plugin/pom.xml
COPY /custom-openapi-generator/modules/openapi-generator-online/pom.xml /custom-openapi-generator/modules/openapi-generator-online/pom.xml
WORKDIR /custom-openapi-generator
RUN mvn -pl '!modules/openapi-generator-online,!modules/openapi-generator-gradle-plugin' dependency:go-offline

WORKDIR /
COPY /custom-openapi-generator/.git /custom-openapi-generator/.git
COPY /custom-openapi-generator/modules /custom-openapi-generator/modules
WORKDIR /custom-openapi-generator
RUN mvn -pl '!modules/openapi-generator-online,!modules/openapi-generator-gradle-plugin' package \
    && cp ./modules/openapi-generator-cli/target/openapi-generator-cli.jar ../openapi-generator-cli.jar

FROM python:3.7-alpine3.9

WORKDIR /

RUN python -m pip install ruamel.yaml

COPY /resolve_yaml.py /resolve_yaml.py
COPY /docker-entry-point.sh /docker-entry-point.sh
COPY --from=builder /openapi-generator-cli.jar /openapi-generator-cli.jar
COPY --from=builder /opt/openjdk-13 /opt/openjdk-13
ENV JAVA_HOME /opt/openjdk-13
ENV PATH $JAVA_HOME/bin:$PATH

RUN apk add --no-cache bash

CMD ["./docker-entry-point.sh"]
