# Customized OpenAPI Generator

This is a fork of the 
[openapi-generator](https://github.com/OpenAPITools/openapi-generator) 
project incorporating 
a customized version of the `python-flask generator`.

## Getting Started

These instructions will get you a copy of the project up and 
running on your local machine.

### Prerequisites

You will need:

* Access to the 
[custom-openapi-generator](https://gitlab.com/maxence.boutet/custom-openapi-generator)
repository with SSH key authentication.
* Docker engine installed on your machine.

### Usage

Build the docker image using:

```
./build-docker-image.sh
```

You might be asked for your SSH private key password.


To generate the server code, the sample in the `test` folder will get you started:

```
./test/generate-python-flask-custom.sh
```
