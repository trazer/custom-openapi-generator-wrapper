#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH=$(dirname "$0")

cd "${SCRIPT_PATH}"

rm -f -r ./custom-openapi-generator

git clone \
    --branch custom-python-flask-generator \
    --depth 1 \
    git@gitlab.com:trazer/custom-openapi-generator.git \
    ./custom-openapi-generator

docker build --tag custom-openapi-generator-wrapper .

rm -f -r ./custom-openapi-generator
