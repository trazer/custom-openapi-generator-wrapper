#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SCRIPT_PATH=$(dirname "$0")

cd "${SCRIPT_PATH}"

git clone \
    --branch custom-python-flask-generator \
    --depth 1 \
    git@gitlab.com:trazer/custom-openapi-generator.git \
    ${CI_PROJECT_DIR}/custom-openapi-generator
