#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

rm -f -r /tmp/custom-openapi-generator
rm -f -r /tmp/openapi-generator

git clone \
    --branch custom-python-flask-generator \
    git@gitlab.com:maxence.boutet/custom-openapi-generator.git \
    /tmp/custom-openapi-generator

git clone \
    --branch master \
    https://github.com/OpenAPITools/openapi-generator.git \
    /tmp/openapi-generator

cd /tmp/custom-openapi-generator
git format-patch origin/master --stdout > /tmp/python_flask_custom.patch

cd /tmp/openapi-generator
git apply --stat --check /tmp/python_flask_custom.patch

rm -f -r /tmp/custom-openapi-generator
rm -f -r /tmp/openapi-generator
