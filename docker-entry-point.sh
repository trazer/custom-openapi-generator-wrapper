#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

function cleanup {
    # Change permission/owner to host user
    chown --recursive ${HOST_UID}:${HOST_GID} ${GENERATED_CODE_FOLDER_PATH}

    # Remove resolved YAML file
    rm -f ${OPENAPI_FOLDER_PATH}/${OPENAPI_FILE_NAME}.resolved.yaml
}

trap cleanup EXIT

# Resolve YAML
python resolve_yaml.py \
    ${OPENAPI_FOLDER_PATH}/${OPENAPI_FILE_NAME} \
    ${OPENAPI_FOLDER_PATH}/${OPENAPI_FILE_NAME}.resolved.yaml

# Generate code
java -jar openapi-generator-cli.jar generate \
    --input-spec ${OPENAPI_FOLDER_PATH}/${OPENAPI_FILE_NAME}.resolved.yaml \
    --generator-name ${GENERATOR_NAME} \
    --output ${GENERATED_CODE_FOLDER_PATH}/${GENERATOR_NAME} \
    --config ${OPENAPI_FOLDER_PATH}/${GENERATOR_CONFIG_FILE_NAME}

# Generate mustache template variables documentation
# https://github.com/openapitools/openapi-generator/wiki/Mustache-Template-Variables
# We generate in a temporary directory otherwise, the generator will fail since
# the output directory is not empty
java -DdebugOperations -DdebugModels \
    -jar openapi-generator-cli.jar generate \
    --input-spec ${OPENAPI_FOLDER_PATH}/${OPENAPI_FILE_NAME}.resolved.yaml \
    --generator-name ${GENERATOR_NAME} \
    --output /tmp/${GENERATOR_NAME} \
    --config ${OPENAPI_FOLDER_PATH}/${GENERATOR_CONFIG_FILE_NAME} \
    > ${GENERATED_CODE_FOLDER_PATH}/mustache-template-variables-${GENERATOR_NAME}.txt
