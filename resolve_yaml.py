import sys
from collections import OrderedDict

import ruamel.yaml
from ruamel.yaml import round_trip_load
from ruamel.yaml.comments import (
    CommentedMap,
    CommentedSeq,
)


def _to_ordered_dict(resolvedYaml):
    assert isinstance(resolvedYaml, CommentedMap)
    convertedResolvedYaml = dict()
    _recursive_build_ordered_dict(resolvedYaml, convertedResolvedYaml)
    return convertedResolvedYaml


def _recursive_build_ordered_dict(mapOrSeq, convertedResolvedYaml):

    if not isinstance(mapOrSeq, (CommentedSeq, CommentedMap)):
        return

    if isinstance(mapOrSeq, CommentedMap):
        for key, value in mapOrSeq.items():
            if isinstance(value, CommentedSeq):
                convertedResolvedYaml[key] = []
            elif isinstance(value, CommentedMap):
                convertedResolvedYaml[key] = dict()
            else:
                convertedResolvedYaml[key] = value
            _recursive_build_ordered_dict(value, convertedResolvedYaml[key])

    elif isinstance(mapOrSeq, CommentedSeq):
        for item in mapOrSeq:
            convertedResolvedYaml.append(item)
            if isinstance(item, CommentedSeq):
                convertedResolvedYaml[-1] = []
            elif isinstance(item, CommentedMap):
                convertedResolvedYaml[-1] = dict()
            _recursive_build_ordered_dict(item, convertedResolvedYaml[-1])


if __name__ == '__main__':

    # From Python 3.7, dict preserves order. We could use
    # OrderedDict, but ruamel.yaml put !!odict tags everywhere
    # when dumping to yaml and openAPI code generator doesn't like it.
    if not (sys.version_info[0] == 3 and sys.version_info[1] == 7):
        raise RuntimeError('Only works with Python 3.7+.')

    inputFileName = sys.argv[1]
    outputFileName = sys.argv[2]

    with open(inputFileName, 'rt') as file:

        resolvedYaml = round_trip_load(file)
        resolvedYaml = _to_ordered_dict(resolvedYaml)

        with open(outputFileName, 'wt') as resolvedFile:
            ruamel.yaml.round_trip_dump(resolvedYaml, stream=resolvedFile, tags=False)
