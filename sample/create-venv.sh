#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

python3.7 -m venv --clear ./.venv
source ./.venv/bin/activate
python -m pip install --upgrade pip
pip install --upgrade setuptools
