#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

cd "$(dirname "$0")"

rm -r -f ./generated_code

LOCAL_OPENAPI_FOLDER_PATH="$(realpath ./)"
LOCAL_GENERATED_CODE_FOLDER_PATH="$(realpath "${GENERATED_CODE_FOLDER}")"

OPENAPI_FOLDER_PATH="/local/openapi"
GENERATED_CODE_FOLDER_PATH="/local/generated_code"

OPENAPI_FILE_NAME="openapi.yaml"

docker run \
    --rm \
    --volume ${LOCAL_OPENAPI_FOLDER_PATH}:${OPENAPI_FOLDER_PATH} \
    --volume ${LOCAL_GENERATED_CODE_FOLDER_PATH}:${GENERATED_CODE_FOLDER_PATH} \
    --env OPENAPI_FOLDER_PATH=${OPENAPI_FOLDER_PATH} \
    --env GENERATED_CODE_FOLDER_PATH=${GENERATED_CODE_FOLDER_PATH} \
    --env OPENAPI_FILE_NAME=${OPENAPI_FILE_NAME} \
    --env GENERATOR_NAME=${GENERATOR_NAME} \
    --env GENERATOR_CONFIG_FILE_NAME=${GENERATOR_CONFIG_FILE_NAME} \
    --env HOST_UID=$(id -u $(whoami)) \
    --env HOST_GID=$(id -g $(whoami)) \
    custom-openapi-generator-wrapper
